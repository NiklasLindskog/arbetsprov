package RSSapp;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class GUI {
	RSSCollector rss;
	JFrame guiFrame;
	JPanel newsPanel;
	JList<String> titles;
	JTextArea newsArea;
	JTextField dateField;

	public GUI() {
		this.rss = new RSSCollector();
		String feed = "";
		try {
			feed = rss.download();
		} catch (IOException e) {
			e.printStackTrace();
		}
		rss.parseRSSFeed(feed);
		guiFrame = new JFrame();
		guiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		guiFrame.setTitle("DN RSS Feed");
		guiFrame.setSize(800, 600);

		newsPanel = new JPanel();
		newsArea = new JTextArea("");
		newsArea.setWrapStyleWord(true);
		newsArea.setLineWrap(true);
		JScrollPane newsAreaPane = new JScrollPane(newsArea);
		newsAreaPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		newsAreaPane.setPreferredSize(new Dimension(380, 300));
		newsPanel.setLayout(new GridLayout(3, 1));

		JPanel newsAreaPanel = new JPanel();
		newsAreaPanel.setLayout(new GridLayout(2, 1));
		JPanel datePanel = new JPanel();
		datePanel.setLayout(new GridLayout(3, 1));
		dateField = new JTextField();
		dateField.setMaximumSize(new Dimension(300, 100));
		newsAreaPanel.add(new JLabel("Description"));
		newsAreaPanel.add(newsAreaPane);
		datePanel.add(new JLabel("Date"));
		datePanel.add(dateField);
		newsPanel.add(datePanel);
		newsPanel.add(newsAreaPanel);

		JPanel titlePanel = new JPanel();
		titles = new JList<String>(rss.fetchTitles());
		JScrollPane titlePane = new JScrollPane(titles);
		titlePane.setPreferredSize(new Dimension(380, 550));
		titlePane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		titles.addListSelectionListener(new NewsSelectionListener());
		titlePanel.add(titlePane);

		JButton button = new JButton("View Full Article");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String title = titles.getSelectedValue();
				String article = null;
				article = rss.fetchEntireArticle(title);
				JFrame page = new JFrame();
				page.setSize(800, 600);
				JTextArea articleArea = new JTextArea(article);
				articleArea.setWrapStyleWord(true);
				articleArea.setLineWrap(true);
				JScrollPane pagePane = new JScrollPane(articleArea);
				page.add(pagePane);
				page.setVisible(true);
			}
		});

		guiFrame.add(titlePanel, BorderLayout.WEST);
		guiFrame.add(newsPanel, BorderLayout.EAST);
		guiFrame.add(button, BorderLayout.SOUTH);
		guiFrame.setVisible(true);
	}

	class NewsSelectionListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {
			if (titles.isSelectionEmpty()) {
				return;
			}
			if (e.getValueIsAdjusting()) {
				String title = titles.getSelectedValue();
				newsArea.setText(rss.fetchDescription(title));
				dateField.setText(rss.fetchDate(title));
			}
		}
	}

}
