package RSSapp;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class RSSCollector {

	HashMap<String, Article> art;
	URL url;

	public RSSCollector() {
		art = new HashMap<String, Article>();
		try {
			url = new URL("http://www.dn.se/nyheter/m/rss/");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	public String download() throws IOException {
		InputStreamReader is = null;
		try {
			is = new InputStreamReader(url.openStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		StringBuilder sb = new StringBuilder();
		int c;
		while ((c = is.read()) != -1) {
			sb.append((char) c);
		}
		return sb.toString();
	}

	public String download(URL url) throws IOException {
		InputStreamReader is = null;
		try {
			is = new InputStreamReader(url.openStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		StringBuilder sb = new StringBuilder();
		int c;
		while ((c = is.read()) != -1) {
			sb.append((char) c);
		}
		return sb.toString();
	}

	public void parseRSSFeed(String feed) {
		String segments[] = feed.split("<item>");
		for (int i = 1; i < segments.length; i++) {
			String item = segments[i];
			URL link = null;
			try {
				link = new URL(getData(item, "link"));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			String title = getData(item, "title");
			art.put(title, new Article(title, getData(item, "description"), link, getData(item, "pubDate")));
		}
	}

	public String parseArticle(String article) {
		int start = article.indexOf("<div class=\"article__body-content\">");
		if (start == -1) {
			return "Article wrongly formatted";
		}
		int currentPosition = start;
		int activeDivTags = 1;
		while (activeDivTags != 0) {
			int starttag = article.indexOf("<div", currentPosition + 1);
			int endtag = article.indexOf("</div", currentPosition + 1);
			if (starttag < endtag) {
				activeDivTags++;
				currentPosition = starttag;
			} else {
				activeDivTags--;
				currentPosition = endtag;
			}

		}
		if (start < 0 || currentPosition < 0) {
			return null;
		}

		article = article.substring(start, currentPosition);
		article = article.replaceAll("\\<[^!>]*>", "").replaceAll("\\{[^}]*}", "")
				.replaceAll("if\\(DN && DN.adLegacy\\)", "").replaceAll("nbsp;", " ");
		return article.trim().replaceAll(" +", " ");

	}

	public String[] fetchTitles() {
		String[] titles = new String[art.size()];
		int i = 0;
		for (String key : art.keySet()) {

			titles[i] = art.get(key).getArticle();
			i++;
		}
		return titles;
	}

	public String fetchEntireArticle(String title) {

		String article = "";
		try {
			article = download(art.get(title).getURL());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return parseArticle(article);
	}

	public String fetchLink(String title) {
		return art.get(title).getURL().toString();
	}

	public String fetchDescription(String title) {
		return art.get(title).getDesc();
	}

	public String fetchDate(String title) {
		return art.get(title).getDate();
	}

	public String getData(String item, String tag) {
		int start = item.indexOf("<" + tag + ">");
		int end = item.indexOf("</" + tag + ">");
		String section = item.substring(start, end);
		section = section.replaceAll("<!\\[CDATA\\[", "");
		section = section.replaceAll("\\]\\]>", "");
		return section.replaceAll("\\<[^!>]*>", "").replaceAll("&nbsp;", " ");
	}

}
