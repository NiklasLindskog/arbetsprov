package RSSapp;

import java.net.URL;

public class Article {
	String title;
	String desc;
	URL url;
	String date;
	
	public Article(String title, String desc, URL url, String date){
		this.title = title;
		this.desc = desc;
		this.url = url;	
		this.date = date;
	}
	
	public String getArticle(){
		return title;
	}
	
	public String getDesc(){
		return desc;
	}
	
	public URL getURL(){
		return url;
	}
	
	public String getDate(){
		return date;
	}
}
